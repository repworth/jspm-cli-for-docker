FROM alpine:latest

RUN \
	apk -Uv add git nodejs \
	&& npm install -g jspm \
	&& rm /var/cache/apk/*